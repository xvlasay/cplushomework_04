Create your own string class (don't use the standard one).
Overload next operators (and more if needed) : "=", "+", "+=", "<<", ">>", "==", "!="
Among the standard function-members also add copy constructor and next functions:
append
compare
length
resize
clear
swap
substr //should search for a specified substring into existing one and return position of the 1st character 
insert //means inserting one character or another string into a specified position

Please create some examples/tests for all these functions
