#include <iostream>
//#include <string>
#include <string.h>
//#include <regex>
//#include <iomanip>
//#include <vector>
using namespace std;
constexpr int MAX = 256;

class my_string
{
private:
	char *arr;
	size_t len = 0;  //real length - with last '\0'

public:
        my_string(const char  str[]): len(strlen(str) + 1) {
		cout<< "constructor: my_string(const char  str[])" << endl;
		arr = new char[len];
		for (int i = 0; i < len; i++) arr[i] = str[i];
		cout << "len = " << len << "; strlen(str) = " << strlen(str) << endl; 
		
	};
        my_string():arr(nullptr), len(0) {
		cout << "constructor: empty object" << endl;
                };
	~my_string() {
		cout << "~my_string(): delete [] arr;" << endl;
		delete [] arr;
		arr = nullptr;
		};
	const char* String(const char  str[]) {
                return str;
                };

	// Copy constructor 
	my_string(const my_string &str2) {
		len = str2.len;
		arr = new char[len];
                for (int i = 0; i < len; i++) arr[i] = str2.arr[i];
	}

	void  operator=(const char  str[]) {
	//	cout << "operator=(const char  str[])" << endl;
		len = strlen(str) + 1;
		if (arr != nullptr) delete [] arr;
		arr = new char[len];
		strcpy(arr, str);
		cout << "operator=: arr = " << arr << endl;
		return;
	}
	my_string operator+(const my_string& str) {
		int i = 0,j = 0, k = 0;
		my_string res;
		res.len = len + str.len - 1;
		res.arr = new char[res.len];
		for ( i = 0; i < MAX; i++) {
			if (i > (len - 2)) break;
			res.arr[i] = arr[i];
		};
		//cout << "1.operator+ :" << res.arr << endl;
		k = 0;
		for (j = i; j < MAX; j++) {
			if (k > (str.len - 1)) break;
			res.arr[j] = str.arr[k++];
		};
		//cout << "2.operator+ :len = " << res.len << endl;
		res.arr[res.len - 1] = '\0';
		//cout << "3.operator+ :" << res.arr << endl;
		return res;
	};
	void  operator+=(const char  str[]) {
		cout << "str: (operator+= ) " << str<< endl;
		cout << "arr:   (operator+= ) " << arr << endl;
		size_t str_len = strlen(str);
                size_t new_len;
                new_len = len + strlen(str);
		char* arr_1;
		arr_1 = new char[new_len];
		strcpy(arr_1, arr);
		strcat(arr_1, str);
		arr_1[new_len - 1] = '\0';
                arr = new char[new_len];
		strcpy(arr,arr_1);
		len = new_len;
		cout << "arr:   (operator+= ) " << arr << endl;
		cout << "len :(operator+= )" << len << endl;
                return;
        }
	void show() {
 		cout << "show: " << endl;
		if (arr != nullptr) {
			cout << arr << endl;
		};
		cout << "len = " << len << endl;
		};
	friend std::ostream& operator<<( std::ostream& output, my_string obj) {
        	 output <<  obj.arr ; // << endl;
        	 return output;
        	}
	friend  void operator>>(string input, my_string &s ) {
                const char* str = input.c_str();
                s = str;
                return ;
        }
        friend  int operator==(my_string &s1, my_string &s2) {
		int retcode;
		switch (strcmp(s1.arr, s2.arr)) {
			case 0:
				retcode = 1;  // true
				break;
			default:
				retcode = 0; // false
		}
                return retcode;
        }
        friend  int operator!=(my_string &s1, my_string &s2) {
                int retcode;
                switch (strcmp(s1.arr, s2.arr)) {
                        case 0:
                                retcode = 0;  // faulsee
                                break;
                        default:
                                retcode = 1; // false
                }
                return retcode;
        }
	// append
        friend void  append(my_string &s1, const char  str[]) {
                s1 += str;
                cout << "append(" << str << "):" << s1.arr << endl;
                return;
        }
	//compare
        friend int  compare(my_string &s1, my_string &s2) {
		return (strcmp(s1.arr, s2.arr));
	}
	//length
	int length() {
		return (len - 1);
                }
	void resize(int new_size) {
		if (new_size > MAX) new_size = MAX;
		char *temp;
		temp = new char[new_size + 1];
		for (int i = 0; i < new_size; i++) {
			if (i < (len - 1)) {
				temp[i] = arr[i];
			} else temp[i] = ' ';
		};
		temp[new_size] = '\0';
		len = new_size + 1;
		delete [] arr;
		arr = new char(len);
		for (int i = 0; i < len; i++) arr[i] = temp[i];
		delete [] temp;
		return;
	}
	void clear() {
		delete [] arr;
		arr = nullptr;
		len = 0;
		return;
	}	
	void swap() {
		char *temp;
		int t_len;
		t_len = len - 1;
                temp = new char[t_len];
                for (int i = 0; i < t_len; i++) {
                	temp[i] = arr[t_len - 1 - i];
                };
		for (int i = 0; i < t_len; i++) {
                        arr[i] = temp[i];
                };
		delete [] temp;
		return;
	}
	int substr(const char  str[]) {
		int i;
		char *ptr;
		ptr = strstr(arr,str);
		if (ptr == nullptr) {
			i = -1;
		} else {
			//i = (int)  (&arr - &ptr);
			i = ptr - arr;
			cout << ptr << endl;
		};
		//cout << "position = " << i << endl;
		return i;
	}
	void insert(int pos, const char  str[]) {
                int i,j;
                char *ptr;
		int newLen;
		int nApp =  0;
		int sLen;
		sLen = strlen(str);
		if (pos > (len - 2)) nApp = 1;
		newLen = len + sLen;
		if (newLen > MAX) newLen = MAX;
		ptr = new char[newLen];
		if (nApp) {
			j = 0;
			for (i = 0; i < (len - 1);i++) {ptr[i] = arr[i]; j++;};
			for (i = (len - 1); i < pos;i++) {ptr[i] = ' '; j++;};
			for (i = 0; i < sLen; i++) ptr[j++] = str[i];
			ptr[newLen - 1] = '\0'; 
		} else {
			j = 0;
			for (i = 0; i < pos;i++) {ptr[i] = arr[i]; j++;};
			for (i = 0; i < sLen; i++) {ptr[j] = str[i]; j++;}; 	
			for (i = pos; i < len - 1; i++) {ptr[j++] = arr[i];};
			ptr[newLen - 1] = '\0'; 
		};
		delete [] arr;
		len = newLen;
		arr = ptr;
                cout << arr << endl;
                return ;
        }
	


};
